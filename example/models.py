from django.contrib.postgres.aggregates import ArrayAgg
from django.db import models

from expression_field.decorator import lazy_expression
from expression_field.field import ExpressionField
from expression_field.model import ExpressionModel


@lazy_expression
def get_posts_name_expression():
    expression = ArrayAgg(
        'posts__name',
        distinct=True,
    )
    return expression


class AuthorQuerySet(models.QuerySet):
    pass


class Author(ExpressionModel):
    objects = AuthorQuerySet.as_manager()

    name = models.CharField(
        verbose_name='Name',
        max_length=1024,
        null=False, blank=False,
    )

    posts_names = ExpressionField(
        get_posts_name_expression()
    )


class PostQuerySet(models.QuerySet):
    pass


class Post(models.Model):
    objects = PostQuerySet.as_manager()

    name = models.CharField(
        verbose_name='Name',
        max_length=1024,
        null=False, blank=False,
    )
    author = models.ForeignKey(
        to='Author',
        related_name='posts',
        null=False, blank=False,
        on_delete=models.CASCADE
    )
