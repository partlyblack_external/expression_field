from .models import *

author_1 = Author.objects.create(name='Author 1')
post_1 = Post.objects.create(name='Post 1', author=author_1)
post_2 = Post.objects.create(name='Post 2', author=author_1)

print(Author.objects.get(name="Author 1").posts_names)
"""
print: ['Post 1', 'Post 2']
"""

print(list(Post.objects.values_list('author__posts_names')))
"""
print: [(['Post 1', 'Post 2'],)]
"""
