from django.db.models.base import ModelBase, Model

from .field import ExpressionField


class Meta(ModelBase):
    @staticmethod
    def _remove_expression_fields(model_cls):
        for field in model_cls._meta.local_fields:
            if isinstance(field, ExpressionField):
                model_cls._meta.local_fields.remove(field)

    def __new__(cls, name, bases, dct):
        x = super().__new__(cls, name, bases, dct)
        cls._remove_expression_fields(x)
        return x


class ExpressionModel(Model, metaclass=Meta):
    class Meta(Meta):
        abstract = True
