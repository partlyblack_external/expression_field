from copy import copy, deepcopy

from django.db.models import OrderWrt
from django.db.models.expressions import Col
from django.db.models.sql.datastructures import Join

from .utils import _add_path, CustomCol


class ExpressionField(OrderWrt):
    def __init__(self, expression, *args, **kwargs):
        kwargs['editable'] = False
        super(OrderWrt, self).__init__(*args, **kwargs)
        self.expression = expression
        self.col_name = ''

    def get_col(self, alias, output_field=None):
        if output_field is None:
            output_field = self
        return CustomCol(Col(alias, self, output_field), self.col_name)

    def get_path(self, compiler):
        db_name = self.model._meta.db_table
        alias = compiler.query.alias_map.get(db_name, None)
        path_lst = []
        if isinstance(alias, Join):
            parent_alias = alias.parent_alias
            field_name = alias.join_field.name
            path_lst.append(field_name)
            while parent_alias:
                path_lst.append(parent_alias)
                alias = compiler.query.alias_map.get(parent_alias, None)
                parent_alias = alias.parent_alias

        if not path_lst:
            return None

        del path_lst[-1]
        path = '__'.join(path_lst[::-1])

        path += '__'
        return path

    def prepare_expression_name(self, path):
        expression = self.expression
        if callable(expression):
            expression = expression()
        if not path:
            return expression, self.name

        expression = copy(expression)
        name = copy(self.name)

        expression = _add_path(deepcopy(expression), path)
        return expression, path + name

    def clear_group_by(self, query):
        if isinstance(query.group_by, list):
            for index, col in enumerate(query.group_by):
                if isinstance(col, CustomCol):
                    query.group_by.pop(index)

    def select_format(self, compiler, sql, params):
        path = self.get_path(compiler)
        expression, name = self.prepare_expression_name(path)
        self.col_name = name
        compiler.query.add_annotation(expression, name, is_summary=False)
        compiler.query.set_group_by()
        self.clear_group_by(compiler.query)
        return compiler.compile(compiler.query.annotation_select.items()[-1][1], select_format=True)
