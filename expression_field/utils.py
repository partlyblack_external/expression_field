from copy import copy

from django.db import models
from django.db.models.expressions import Col, Subquery


class CustomCol(Col):
    def __init__(self, col, name, output_field=None):
        alias = copy(col.alias)
        target = copy(col.target)
        target.column = name
        super(CustomCol, self).__init__(alias, target, output_field)

    def as_sql(self, compiler, connection):
        qn = compiler.quote_name_unless_alias
        return "%s" % qn(self.target.column), []

    def get_group_by_cols(self):
        return []


def _add_path(expression, path):
    if isinstance(expression, tuple) and len(expression) == 2:
        expression = list(expression)
        expression[0] = path + expression[0]
        expression = tuple(expression)
        return expression
    elif isinstance(expression, models.F):
        expression.name = path + expression.name
    elif isinstance(expression, models.Q):
        expression.children = tuple((_add_path(expr, path) for expr in expression.children))
    elif isinstance(expression, Subquery):
        raise ValueError('Not supported')
    else:
        for expr in expression.get_source_expressions():
            _add_path(expr, path)
    return expression
