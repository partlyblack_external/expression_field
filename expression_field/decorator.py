def lazy_expression(func):
    """
    Used in situations where Django throws an exception
    """
    def wrapper(*args, **kwargs):
        return lambda: func(*args, **kwargs)

    return wrapper
